
**What**

Fed with compounds of interest, `Minery` is dedicated to fetch bioassay data
from available database, for example, PubChem.

**Use**

Go to folder <kbd>BBB_server</kbd> and launch R console.

``` r
> library(shiny)
> runApp()
```

Or run in cloud <https://yanyun.shinyapps.io/minery/>
