## =====================
## multiGetBioassay is R function
## 
## INPUT: file recording CID without headers
## 
## OUTPUT:
## format: 
## [[1]] indicator: 
## [[2]] multi:    data.table | num vector | 0 
## =====================

library('reshape2')
library('reshape')
library('data.table') ## data.table is faster than data.frame
library('dplyr')
library('plyr')

factorizeActivity <- function(x) {
## FUNCTION: convert character label as numeric label as categorical data
## INPUT: character vecor
## OUTPUT: factor

## Refer to : https://pubchem.ncbi.nlm.nih.gov/help.html#sAssayheat
## Activity Outcome includes 6 results: 
## Probe, Active, Inactive, Unspecified/Inconclusive, or Untested. 
	PROBE_LABEL        <- 2
	ACTIVE_LABEL       <- 1
	INACTIVE_LABEL     <- -1
	UNSPECIFIED_LABEL  <- 0.5
	INCONCLUSIVE_LABEL <- -0.5
	UNTESTED_LABEL     <- -2

	output <- factor(x, 
			  levels = c("Probe", "Active", "Inactive", 
			  			 "Unspecified","Inconclusive", "Untested"), 
			  labels = c(PROBE_LABEL, ACTIVE_LABEL, INACTIVE_LABEL, 
			  	         UNSPECIFIED_LABEL, INCONCLUSIVE_LABEL, UNTESTED_LABEL)
			  )
	output
}

reshapeDf2Dt <- function(df){
## FUNCTION: reshape data.frame to targeted shape formated in data.table
## INPUT: data.frame with 3 columns: 
## CID | AID | Bioactivity.Outcome

## OUTPUT: data.table
## example: 
## CID | AID1 | AID20 ... 
## 180 | Active | Inconclusive
	# df$Bioactivity.Outcome <- factorizeActivity(df$Bioactivity.Outcome)
	# dt <- as.data.table(df[1:5, ]) ## !!!TEST!!! 
	dt <- as.data.table(df)
	dt <- dcast(dt, CID ~ AID, fill = NA, drop = FALSE)
	dt
}

multiGetBioassay <- function(file, colNumID = 1){
## FUNCTION: cook CId~AID big data.table
## INPUT: file recording CIDs
## OUTPUT: data.table 
## 		   With CID as rownames, AID as colnames, 
##         bioactivity.outcome as value in each cell. 

	cat("## Running multiGetBioassay function\n")
	identifiers <- read.table(file, quote="\"", stringsAsFactors=FALSE)
	identifiers <- as.numeric(identifiers[, colNumID])
	totalSizeIds <- length(unique(identifiers))
	cat(paste0("## Number of requested unique identifiers: ", totalSizeIds, "\n"))

	## 1. Preparation
	tempLocalFile <- tempfile()

	## 2. Big list storing result produced by monoGetBioassay() function
	cat("## Saving \n")
	## either lapply(base) or llply(plyr)
	bsData <- llply(identifiers, function(x) monoGetBiossay(x, tempLocalFile))
	## list
	## [[1]]: list
	##         [[1]]: indicator
	##         [[2]]: 0 | num | data.frame

	## 3. Selection CID with non-empty bioassays
	PASS_RUN_FLAG <- 1
	passIndi <- laply(bsData, function(x) {
									o <- (get("flag", x) == PASS_RUN_FLAG) ## TRUE/FALSE
									o
								}
					  )
	
	bsDataPass <- llply(bsData[passIndi], function(x) {
											reshapeDf2Dt(get("value", x))
											}
						)
	## list
	## [[1]]: data.table

	bsDataFail <- laply(bsData[!passIndi], function(x) {
											get("value", x)
											}
						)
	## numberic vector recording CIDs

	cat("## CID without any reported bioassays: \n")
	cat(paste0(bsDataFail, "\n"))

	## 4. Output
	rbindlist(bsDataPass, fill = TRUE)
}

exportCSV <- function(x){
## FUNCTION: export data for download
## INPUT: data.table
## OUTPUT: csv file
}

