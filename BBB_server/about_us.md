### About CIIP toolkit

CIIP (Chemical In Vitro In Vivo Profilling) is a toolkit with ultimate goal to model the similarities among the bio-activity of small molecules and high-throughput screening data. It is promising for researches in chemeoinformatics and drug discovery.

CIIP is composed of several independent modules: 

- **Minery**. Fed with compounds of interest, `Minery` is dedicated to fetch bioassay data from available database, for example, PubChem. 
- **Matry**.  Once data is retrieved a big sparse matrix would be produced with compounds as rows and bioassays as columns. `Matry` is focused on cooking the matrix, feature reduction, incubate the model. 
- **Pretty**. Given the model produced by `Matry`, users could use the model to predict potential bioactivity of unknown compound by using `Pretty`.

Users could choose the starting step based on your status. 

CIIP is user friendly especially to those who may not be familiar with coding. Not only as on-line tool, it is flexible and easily accessible to run in your own computer and server. 

### About Us

This is xxxx Lab at xxxxx University. 

Yun Yan created this website tool which is academic free. Proud of using `R` and Shiny server.  

If you have any question or are willing to collaborate with us, don't hesite drop us an email: xxxxx at xxxxx dot edu, please. 

### News

- `Minery` is on-line. 


### TO-DO

- `Minery` is running on Shiny server. Without access to edit the configuration settings of server, `Minery` will be forced to shut down once one round of reactive processing takes more than 1 minute, i.e. fetching bioassays data from PubChem cloud. So far, `Minery` on-line could perfectly support 100 compounds though no limits is found if running on local server. 
- Running log files should be turned in to users, though it is available on local server. 


